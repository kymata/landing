/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		colors: {
			'blue': {
				'50': '#edfdfe',
				'100': '#d0f7fd',
				'200': '#a7edfa',
				'300': '#6bdef5',
				'400': '#42cceb',
				'500': '#0ca8ce',
				'600': '#0d86ad',
				'700': '#126c8c',
				'800': '#185872',
				'900': '#184961',
				'950': '#011013',
			},
			'gray': {
				'50': '#f9fafb',
				'100': '#f3f4f6',
				'200': '#e5e7eb',
				'300': '#d1d5db',
				'400': '#9ca3af',
				'500': '#6b7280',
				'600': '#4b5563',
				'700': '#374151',
				'800': '#1f2937',
				'900': '#111827',
				'950': '#030712',
			},		
			'green': {
				'50': '#effefa',
				'100': '#c9fef4',
				'200': '#93fcea',
				'300': '#55f3dc',
				'400': '#36e2cf',
				'500': '#09c3b2',
				'600': '#049d91',
				'700': '#087d76',
				'800': '#0c635e',
				'900': '#0f524e',
				'950': '#013232',
			},
			'pink': '#EB4AAF',
			'white': '#f4f9fb'
		},
		container: {
			center: true,
			padding: '2rem'
		},
	},
	plugins: [],
}
